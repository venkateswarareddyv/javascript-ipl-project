const fs = require("fs");
const { parse } = require('csv-parse');

let result = [];

fs.createReadStream('../data/matches.csv').pipe(parse({columns:true,dslimiter:','}))
    .on('data',function(data){
        result.push(data)
    }).on('end',function(){
        let obtainedValue=topPlayerPerSeason(result);
        let writeStream=fs.createWriteStream('../public/output/6-player-of-the-match-of-season.json')
        writeStream.write(JSON.stringify(obtainedValue));
        writeStream.end();
    })



function topPlayerPerSeason(array){
    let toGetPlayerData=array.reduce(function(initialObject,objectcoming){
        let seasons=objectcoming.season
        if(!initialObject[seasons]){
            initialObject[seasons]={}
        }else{
            let player=objectcoming.player_of_match
            if(!initialObject[seasons][player]){
                initialObject[seasons][player]=1
            }else{
                initialObject[seasons][player] +=1
            }
        }
        return initialObject;
    },{})
    let Players={}
    for(let years in toGetPlayerData){
        let objectToArray=Object.entries(toGetPlayerData[years]).sort((a,b)=>b[1]-a[1]).slice(0,1);
        let arrayTobject=Object.fromEntries(objectToArray)
        Players[years]=arrayTobject;
    }
    return Players;
}
