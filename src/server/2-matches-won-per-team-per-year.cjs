const fs = require("fs");
const { parse } = require('csv-parse');

let result = [];

fs.createReadStream('../data/matches.csv').pipe(parse({columns:true,dslimiter:','}))
    .on('data',function(data){
       result.push(data)
    }).on('end',function(){
        let matchWonbyTeamPerYear=matchesWonPerTeamPerYear(result)
       let writeStream=fs.createWriteStream('../public/output/2-matches-won-per-team-per-year.json')
       writeStream.write(JSON.stringify(matchWonbyTeamPerYear));
       writeStream.end();
    })

function matchesWonPerTeamPerYear(array){
    let objectOfObjects={}
    for(let i=0;i<array.length;i++){
        let object=array[i].season
        if(!objectOfObjects[object]){
            objectOfObjects[object]={}
        }   
    }
    for(let key in objectOfObjects){
        for(let i=0;i<array.length;i++){
            if(key===array[i].season){
                let winner1=array[i].winner;
                if(!objectOfObjects[key][winner1]){
                    objectOfObjects[key][winner1]=1
                }else{
                    objectOfObjects[key][winner1] +=1
                }
            }
        }
    }
    return objectOfObjects;
}
