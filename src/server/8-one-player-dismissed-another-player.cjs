const fs = require("fs");
const { parse } = require('csv-parse');

let result = [];

fs.createReadStream('../data/deliveries.csv').pipe(parse({columns:true,dslimiter:','}))
    .on('data',function(data){
        result.push(data)
    }).on('end',function(){
        let obtainedObject=toGetCountDismisedPlayer(result)
        let writeStream=fs.createWriteStream('../public/output/8-one-player-dismissed-another-player.json')
        writeStream.write(JSON.stringify(obtainedObject));
        writeStream.end();
    })

function toGetCountDismisedPlayer(arrayOfObjects){
  let obtained=arrayOfObjects.reduce((initialObject,objectFromArrayOfObject)=>{
    if(objectFromArrayOfObject.player_dismissed !=="" && objectFromArrayOfObject.dismissal_kind!=="run out" && objectFromArrayOfObject.dismissal_kind!=="retired hurt"){
      let player=objectFromArrayOfObject.player_dismissed
      let bowler=objectFromArrayOfObject.bowler
      if(!initialObject[player]){
        initialObject[player]={};
        initialObject[player][bowler]=1;
      }else{
        if(!initialObject[player][bowler]){
          initialObject[player][bowler]=1
        }
        initialObject[player][bowler]=initialObject[player][bowler]+1
      }

    }
    return initialObject;
  },{})
  let sortedBasedonDismissal={}
  for(let batsman in obtained){
    let covertingToArrayToSort = Object.entries(obtained[batsman]).sort((a,b)=>b[1]-a[1]);
    let newobject=Object.fromEntries(covertingToArrayToSort)
    sortedBasedonDismissal[batsman]=newobject;
  }
  return sortedBasedonDismissal;
}
