const fs = require("fs");
const { parse } = require('csv-parse');

let result = {};
let deliversResult=[]

fs.createReadStream('../data/matches.csv').pipe(parse({columns:true,dslimiter:','}))
    .on('data',function(data){
        if(!result[data.season]){
            result[data.season]=[data.id]
        }else{
            result[data.season].push(data.id)
        }
    }).on('end',function(){
        fs.createReadStream('../data/deliveries.csv').pipe(parse({columns:true,dslimiter:','}))
            .on('data',function(data){
                deliversResult.push(data)
            }).on('end',function(){
                let obtainedValues=toObtainStrikeRate(result,deliversResult);
                let writeStream=fs.createWriteStream('../public/output/7-strike-rate-of-a-batsman-per-year.json')
                writeStream.write(JSON.stringify(obtainedValues));
                writeStream.end();
            })
    })

function toObtainStrikeRate(object,arrays){
    let deliveriesAndRuns={}
    for(let i=0;i<arrays.length;i++){
        for(let key in object){
            for(let j=0;j<object[key].length;j++){
                if(object[key][j]===arrays[i].match_id){
                    if(!deliveriesAndRuns[key]){
                        deliveriesAndRuns[key]={}
                    }else{
                        let player=arrays[i].batsman
                        if(!deliveriesAndRuns[key][player]){
                            deliveriesAndRuns[key][player]={"deliveries":1,"runs":parseInt(arrays[i].batsman_runs)}
                        }else{
                            deliveriesAndRuns[key][player].deliveries +=1;
                            deliveriesAndRuns[key][player].runs=deliveriesAndRuns[key][player].runs+parseInt(arrays[i].batsman_runs)
                        }
                    }
                }
            }
        }
    }
    let strikeRateBasedonYears={}
    for (let years in deliveriesAndRuns){
        for(let batsman in deliveriesAndRuns[years]){
            let strikeRate=Math.round((deliveriesAndRuns[years][batsman].runs/deliveriesAndRuns[years][batsman].deliveries)*100)
            if(!strikeRateBasedonYears[years]){
                strikeRateBasedonYears[years]={}
            }else{
                let forCheckingBatsman=strikeRateBasedonYears[years]
                if(!forCheckingBatsman[batsman]){
                    forCheckingBatsman[batsman]=strikeRate;
                }
            }
        }
    }
    return strikeRateBasedonYears;
}