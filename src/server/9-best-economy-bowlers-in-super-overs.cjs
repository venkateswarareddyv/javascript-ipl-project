const fs = require("fs");
const { parse } = require('csv-parse');

let result = [];

fs.createReadStream('../data/deliveries.csv').pipe(parse({columns:true,dslimiter:','}))
    .on('data',function(data){
       result.push(data)
    }).on('end',function(){
        let highEconomyPlayer=togeteconomyBowlers(result)
       let writeStream=fs.createWriteStream('../public/output/9-best-economy-bowlers-in-super-overs.json')
       writeStream.write(JSON.stringify(highEconomyPlayer));
       writeStream.end();
    })

function togeteconomyBowlers(result){
    let player={}
    for(let i=0;i<result.length;i++){
        if(result[i].is_super_over!=="0"){
            if(!player[result[i].bowler]){
                if(result[i].wide_runs == 1 || result[i].noball_runs == 1){
                    player[result[i].bowler]={"runs":parseInt(result[i].total_runs)-parseInt(result[i].legbye_runs)-parseInt(result[i].bye_runs),"balls":0}
                }
                else{
                    player[result[i].bowler]={"runs":parseInt(result[i].total_runs)-parseInt(result[i].legbye_runs)-parseInt(result[i].bye_runs),"balls":1}
                }
            }
            else{
                if(result[i].wide_runs == 1 || result[i].noball_runs == 1){
                    player[result[i].bowler].runs=player[result[i].bowler].runs+(parseInt(result[i].total_runs)-parseInt(result[i].legbye_runs)-parseInt(result[i].bye_runs))
                    player[result[i].bowler].balls=player[result[i].bowler].balls
                }
                else{
                    player[result[i].bowler].runs=player[result[i].bowler].runs+(parseInt(result[i].total_runs)-parseInt(result[i].legbye_runs)-parseInt(result[i].bye_runs))
                    player[result[i].bowler].balls=player[result[i].bowler].balls+1

                }
            }
        }
        
    }
    let final={}
    for(let individualPlayer in player){
        if(!final[individualPlayer]){
            final[individualPlayer]= Math.round(player[individualPlayer].runs/(player[individualPlayer].balls/6))
        }
    }

    let objectToFindTop=Object.entries(final).sort((a,b)=>a[1]-b[1]).slice(0,1);
    let arrayToObject=Object.fromEntries(objectToFindTop);
    return arrayToObject;
}