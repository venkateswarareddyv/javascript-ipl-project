const fs = require("fs");
const { parse } = require('csv-parse');

let result = {};

fs.createReadStream('../data/matches.csv').pipe(parse({columns:true,dslimiter:','}))
    .on('data',function(data){
        toGetMatchesPlayedInaYear(data);
    }).on('end',function(){
       let writeStream=fs.createWriteStream('../public/output/1-matches-per-year.json')
       writeStream.write(JSON.stringify(result));
       writeStream.end();
    })

function toGetMatchesPlayedInaYear(object){
    let year=object.season
    if(!result[year]){
        result[year]=1;
    }else{
        result[year]=result[year]+1
    }

}




    