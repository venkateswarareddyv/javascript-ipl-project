const fs = require("fs");
const { parse } = require('csv-parse');

let result = [];

fs.createReadStream('../data/matches.csv').pipe(parse({columns:true,dslimiter:','}))
    .on('data',function(data){
        result.push(data)
    }).on('end',function(){
       let winsTeam=bothWins(result);
       let writeStream=fs.createWriteStream('../public/output/5-toss-win-and-watch-win.json')
       writeStream.write(JSON.stringify(winsTeam));
       writeStream.end();
    })

function bothWins(result){
    let outputObject={}
    for(let i=0;i<result.length;i++){
        if(result[i].toss_winner===result[i].winner){
            let team=result[i].toss_winner;
            if(!outputObject[team]){
                outputObject[team]=1;
            }else{
                outputObject[team] +=1;
            }
        }
    }
    return outputObject;
}

