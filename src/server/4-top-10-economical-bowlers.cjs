const fs = require("fs");
const { parse } = require('csv-parse');

let result = [];
let deliversResult=[]


fs.createReadStream('../data/matches.csv').pipe(parse({columns:true,dslimiter:','}))
    .on('data',function(data){
        if(data.season==="2015"){
            result.push(data.id)
        }
    }).on('end',function(){
        fs.createReadStream('../data/deliveries.csv').pipe(parse({columns:true,dslimiter:','}))
            .on('data',function(data){
                deliversResult.push(data)
            }).on('end',function(){
                let economyTopBowlers=toGettop10EconomicalBowlers(result,deliversResult)
                let writeStream=fs.createWriteStream('../public/output/4-top-10-economical-bowlers.json')
                writeStream.write(JSON.stringify(economyTopBowlers));
                writeStream.end();
            })
    })

function toGettop10EconomicalBowlers(result,deliversResult){
    let bowlersObject={};
    for(let i=0;i<deliversResult.length;i++){
        for(let j=0;j<result.length;j++){
            if( deliversResult[i].match_id===result[j]){
                let bowler=deliversResult[i].bowler
                if(!bowlersObject[bowler]){
                    bowlersObject[bowler]={balls:0,runs:0};
                }else{
                    let presentBowler=bowlersObject[bowler];
                    presentBowler.balls=presentBowler.balls+1;
                    presentBowler.runs=presentBowler.runs+parseInt(deliversResult[i].total_runs)
                }
            }
        }
    }
    for(let key in bowlersObject){
        let overs=bowlersObject[key].balls/6;
        let runs=bowlersObject[key].runs;
        let calculatingAverage=runs/overs;
        bowlersObject[key]["average"]=parseFloat(calculatingAverage).toFixed(2);
    }
    
    let sortedData = Object.entries(bowlersObject).sort((firstData,secondData)=>{
        return firstData[1].average - secondData[1].average;
    });
    return (Object.fromEntries(sortedData.slice(0,10)));
    
}
