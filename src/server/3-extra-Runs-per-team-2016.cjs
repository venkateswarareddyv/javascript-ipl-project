const fs = require("fs");
const { parse } = require('csv-parse');

let result = [];
let deliversResult=[]


fs.createReadStream('../data/matches.csv').pipe(parse({columns:true,dslimiter:','}))
    .on('data',function(data){
        if(data.season==="2016"){
            result.push(data.id)
        }
    }).on('end',function(){
        fs.createReadStream('../data/deliveries.csv').pipe(parse({columns:true,dslimiter:','}))
            .on('data',function(data){
                deliversResult.push(data)
            }).on('end',function(){
                let overRuns=toGetExtraRunsForTeam(result,deliversResult);
                let writeStream=fs.createWriteStream('../public/output/3-extra-runs-per-team-2016.json')
                writeStream.write(JSON.stringify(overRuns));
                writeStream.end();
            })
    })



function toGetExtraRunsForTeam(matchIdArray,deliversResult){
    let deliveriesTeam={};
    for(let i=0;i<deliversResult.length;i++){
        for(let j=0;j<matchIdArray.length;j++){
            if(deliversResult[i].match_id===matchIdArray[j]){
                let team=deliversResult[i].bowling_team
                if(!deliveriesTeam[team]){
                   deliveriesTeam[team]=parseInt(deliversResult[i].extra_runs)
                }else{
                    deliveriesTeam[team]=deliveriesTeam[team]+parseInt(deliversResult[i].extra_runs)
                }
            }
            
        }
    }
    
    return deliveriesTeam;
}

